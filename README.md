# NovaClients File Storage Service

## Installation

Service requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and devDependencies and start the service. It will also run a [moleculer cli](https://moleculer.services/docs/0.13/moleculer-cli.html), so you can call actions and do other interesting stuff.

```sh
$ cd service-app
$ yarn install
$ yarn run dev
```

## Contributing

This project is designed to use several useful services and tools:

- [eslint](https://eslint.org/)
- [conventional-changelog](https://github.com/conventional-changelog/conventional-changelog)
- [standard-version](https://github.com/conventional-changelog/standard-version#readme)

Before making a commit you should install [cz-cli](https://github.com/commitizen/cz-cli):

```sh
$ npm install -g commitizen cz-conventional-changelog
```

When you commit with Commitizen, you'll be prompted to fill out any required commit fields at commit time. No more waiting until later for a git commit hook to run and reject your commit (though that can still be helpful). Get instant feedback on your commit message formatting and be prompted for required fields.

[![Add and commit with Commitizen](https://github.com/commitizen/cz-cli/raw/master/meta/screenshots/add-commit.png)](https://github.com/commitizen/cz-cli/raw/master/meta/screenshots/add-commit.png)
