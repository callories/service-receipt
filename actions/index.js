const pkg = require('./../package.json')

module.exports = {
  version: () => pkg.version,
  'object.get': require('./object.get'),
}
