const { ValidationError } = require('moleculer').Errors

class BucketAlreadyExistsError extends ValidationError {
  constructor (msg, data) {
    super(msg || 'Your previous request to create the named bucket succeeded and you already own it.', 'BUCKET_ALREADY_EXISTS_ERROR', data)
  }
}

module.exports = BucketAlreadyExistsError
