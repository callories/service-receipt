const DbService = require('moleculer-db')
const MongoDBAdapter = require('moleculer-db-adapter-mongo')

const actions = require('./actions')
const methods = require('./methods')
const hooks = require('./hooks')
const lifecycles = require('./lifecycles')

const ProductService = {
  name: 'receipt',

  mixins: [DbService],
  adapter: new MongoDBAdapter('mongodb://mongo/calories'),
  collection: 'receipts',
  settings: {
    populates: {
      // Shorthand populate rule. Resolve the `voters` values with `users.get` action.
      // "products": "product.get",
      products (products, receipts, rule, ctx) {
        return this.Promise.all(receipts.map(async receipt => receipt.products = await Promise.all(receipt.products.map(async product => {
          const pr = await ctx.call('product.get', { id: product.id })
          return Object.assign(product, pr)
        }))))
      },
      // Define the params of action call. It will receive only with username & full name of author.
      author: {
        action: 'users.get',
        params: {
          fields: 'username fullName',
        },
      },
    },
  },
  ...lifecycles,
  actions,
  methods,
  hooks,
}

module.exports = ProductService
